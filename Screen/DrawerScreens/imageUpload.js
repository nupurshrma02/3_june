import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'react-native-axios';
let data = new FormData();

export default class imageUpload extends Component {
  constructor(props) {
    super(props)
    this.state = {
        formDataArray: [],
        userid: '',
        image1 : null,
        image2 : null,
        image3 : null,
        imageData1: null,
        imageData2: null,
        imageData3: null,
        imageFilename1: '',
        imageFilename2: '',
        imageFilename3: '',
    }
   }

  componentDidMount() {
    this.checkingAsync();
  }

  checkingAsync = async () => {
    try {
      let user = await AsyncStorage.getItem('userdetails');
      user = JSON.parse(user);
      this.setState({userid : user.id})
      console.log('user id=>', user.id)
      console.log('userData =>', user);
    } catch (error) {
      console.log(error);
    }
  };

  selectingImage1 = () => {
    ImagePicker.showImagePicker({quality: 0.3}, responseGet => {
      console.log('Response = ', responseGet);

      if (responseGet.didCancel) {
        console.log('User cancelled image picker');
      } else if (responseGet.error) {
        console.log('ImagePicker Error: ', responseGet.error);
      } else {
        const source = {uri: responseGet.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          image1: source,
          imageData1: responseGet.data,
          imageFilename1: responseGet.fileName,
        });
      }
    });
  };

  selectingImage2 = () => {
    ImagePicker.showImagePicker({quality: 0.3}, responseGet => {
      console.log('Response = ', responseGet);

      if (responseGet.didCancel) {
        console.log('User cancelled image picker');
      } else if (responseGet.error) {
        console.log('ImagePicker Error: ', responseGet.error);
      } else {
        const source = {uri: responseGet.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          image2: source,
          imageData2: responseGet.data,
          imageFilename2: responseGet.fileName,
        });
      }
    });
  };

  selectingImage3 = () => {
    ImagePicker.showImagePicker({quality: 0.3}, responseGet => {
      console.log('Response = ', responseGet);

      if (responseGet.didCancel) {
        console.log('User cancelled image picker');
      } else if (responseGet.error) {
        console.log('ImagePicker Error: ', responseGet.error);
      } else {
        const source = {uri: responseGet.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          image3: source,
          imageData3: responseGet.data,
          imageFilename3: responseGet.fileName,
        });
      }
    });
  };

  upload = () => {
    fetch('http://3.12.158.241/medical/api/image/'+this.state.userid,{
      method: "POST",
      headers: {
          'Accept' : "application/json",
          'Content-Type': 'application/multipart/form-data'
      },      
      body: JSON.stringify(data.append({'image[]':[
        {
          name: 'image1',
          filename: this.state.imageFilename1,
          type: 'image/jpg',
          data: this.state.imageData1,
        },
        {
          name: 'image2',
          filename: this.state.imageFilename2,
          type: 'image/jpeg',
          data: this.state.imageData2,
        },
        {
          name: 'image3',
          filename: this.state.imageFilename3,
          type: 'image/jpeg',
          data: this.state.imageData3,
        },
      ]}))

     }).then((res) => res.json())
    .then(resData => {
      console.log(resData);
      console.log('success');
      alert("Success")
    }).catch(err => {
    console.error("error uploading images:", err);
    //console.log('datttttaa--->', JSON.stringify(data));
    })
        
  };

  upload1 = () => {
    RNFetchBlob.fetch(
      'POST',
      'http://3.12.158.241/medical/api/image/'+this.state.userid,
      {
        //Authorization: 'Bearer access-token',
        //otherHeader: 'foo',
        'Content-Type': 'multipart/form-data, application/json',
      },
      [
        {
          name: 'image1',
          filename: this.state.imageFilename1,
          type: 'image/jpg',
          data: this.state.imageData1,
        },
        {
          name: 'image2',
          filename: this.state.imageFilename2,
          type: 'image/jpeg',
          data: this.state.imageData2,
        },
        {
          name: 'image3',
          filename: this.state.imageFilename3,
          type: 'image/jpeg',
          data: this.state.imageData3,
        },
      ],
    )
      .then(resp => {
        //this.setState({isLoading: false});
        console.log('After Posting =>');
        console.log(resp);
        //this.setModalVisible(!this.state.modalVisible);
        //this.props.navigation.navigate('ListingView');
        alert('success')
      })
      .catch(err => {
        console.log('Error =>' + err);
        //this.setState({isLoading: false});
      });
  }

  upload2 = () => {
    const config = {
      method: 'post',
      headers: {
        //'NEEDS_AUTH': true,
        Accept: 'application/json',
        'Content-type': 'multipart/form-data'

      },
      formDataArray : JSON.stringify([
        {
        name: 'image1',
        filename: this.state.imageFilename1,
        type: 'image/jpg',
        data: this.state.imageData1,
      },
      {
        name: 'image2',
        filename: this.state.imageFilename2,
        type: 'image/jpeg',
        data: this.state.imageData2,
      },
      {
        name: 'image3',
        filename: this.state.imageFilename3,
        type: 'image/jpeg',
        data: this.state.imageData3,
      },
      ]
    ),
      url: 'http://3.12.158.241/medical/api/image/'+this.state.userid,
      // data: {
      //   title: postTitle,
      //   content: postContent,
      //   location: locationId,
      //   category: categoryId,

      // }
    }

    axios(config).then(res => console.log('create post ', res)).catch(err => console.log('create post err', err.response)) 
  }
  

  render() {
    return (
      <View>
      <View style={{flexDirection:'row', justifyContent:'center'}}>
        <TouchableOpacity onPress={this.selectingImage1}>
         <View style={styles.imageContainer}>
         <Image
                      source={
                        this.state.image1 === null
                          ? require('../../Image/plus.png')
                          : this.state.image1
                      }
                      style={{height: 50, width:50}}
                    />
         </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.selectingImage2}>
         <View style={styles.imageContainer}>
         <Image
                      source={
                        this.state.image2 === null
                          ? require('../../Image/plus.png')
                          : this.state.image2
                      }
                      style={{height: 50, width:50}}
                    />
         </View>
         </TouchableOpacity>
         <TouchableOpacity onPress={this.selectingImage3}>
         <View style={styles.imageContainer}>
         <Image
                      source={
                        this.state.image3 === null
                          ? require('../../Image/plus.png')
                          : this.state.image3
                      }
                      style={{height: 50, width:50}}
                    />
         </View>
         </TouchableOpacity>
        </View>
        <View>
           <TouchableOpacity onPress={this.upload}> 
             <Text>Upload</Text>
           </TouchableOpacity>
        </View>
        </View>
      
    );
  }
}

const styles = StyleSheet.create({
    imageContainer:{
      position: 'relative',
      marginTop: 10,
      marginLeft: 10,
      padding: 40,
      backgroundColor: '#fff',
      //height: 120,
      //width: 120,
    },
});
